"""A Pygments style for people with Deuteranopia."""

from setuptools import setup, find_packages
setup(
    name = "Deuteranopia",
    version = "0.1",
    packages = find_packages(),

    # Project uses reStructuredText, so ensure that the docutils get
    # installed or upgraded on the target machine
    install_requires = ['docutils>=0.3'],

    package_data = {
        # If any package contains *.txt or *.rst files, include them:
        '': ['*.txt', '*.rst'],
    },

    # metadata for upload to PyPI
    author = "Evan Cofsky",
    author_email = "evan@theunixman.com",
    description = "A Pygments style for both normal and deuteranopic vision.",
    license = "GPL-3.0+",
    keywords = "pygments styles code formatting",
    url = "https://gitlab.com/theunixman/publication/deuteranopia",
    entry_points={"pygments.styles":
                  "deuteranopia = Deuteranopia:Deuteranopia"}
)
