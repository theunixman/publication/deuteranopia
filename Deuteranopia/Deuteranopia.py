"""Pygments style deuteranopic vision."""

from pygments.style import Style
from pygments.token import Keyword, Name, Comment, String, Error, \
     Number, Operator, Generic

__all__ = ["Deuteranopia"]

class Deuteranopia(Style):
    """Highly contrasting colors for red/green colorblindness.

    Also ensures that contrasts for normally-sighted people are
    sufficient."""

    default_style = ""

    styles = {

        # Blues:
        #
        # Blues are used for names, values, functions, types, and
        # other programmer-defined things.

        ## General names are a deep blue.
        Name:                   '003e68',

        ## Functions are a lighter blue.
        Name.Function:          '#008ce2',


        ## Type names are bold blue.
        Keyword.Type:           'bold #008de8'

        ## Namespaces are a lighter but still bold blue
        Keyword.Namespace:      'bold #b9c4ff'

        # Yellows:
        #
        # Yellows are for operators, language built-ins, and pragmas
        # where available.

        ## Reserved words are a bold, deep yellow.
        Keyword.Reserved:       'bold #ffd080',

        # Operators are a darker, softer yellow.
        Operator:               '#a67f00'
        Punctuation:            '#a67f00'

        # Neutrals
        #
        # Strings, comments, and documentation are colored with Neutrals.

        ## Comments are slightly lower contrast on a white background.
        Comment:                '#cebdcf',

        ## Strings are a darker neutral.
        String:                 '#a4876a',

        ## Characters are the same as String but bold.
        String.Char:            'bold #a4876a',

        ## Numbers are lighter and bold.
        Number:                 'bold #e0ac2e',

        # Text is Black
        Text:                   '#000000',

        # Error is Red
        Error:                  'bold #c09300',
    }
